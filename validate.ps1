Write-Host "Install PSScriptAnalyzer"
Install-Module PSScriptAnalyzer -Scope CurrentUser -Confirm:$False -Force
#Write-Host $(Get-ScriptAnalyzerRule)
Write-Host "Check testscript.ps1"
$result = Invoke-ScriptAnalyzer -Path "scripts\" -Recurse
if ($result.Length -gt 0) {
  Write-Host "Script Error"
  Write-Host ($result | Format-Table | Out-String)
  #throw "Script analysis failed"
  exit 1;
}

